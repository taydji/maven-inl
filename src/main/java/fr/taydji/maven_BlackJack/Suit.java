

// ENUM qui represente les possibles suits des cartes
public enum Suit {
    Clubs,
    Diamonds,
    Spades,
    Hearts,
    
}

