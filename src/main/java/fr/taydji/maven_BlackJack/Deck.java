import java.util.Random;

public class Deck {

    // informations de la classe

    private Card[] myCards;
    private int numdeCards;
    
    //public Deck() {
	 // tom konstruktor överloading ( default to shuffle 52 cards)
	
    //}

    // constructeur qui definit les nombres de cartes

    public Deck(int numDeck, Boolean shuffle) {// deck constructeur

	this.numdeCards = numDeck * 52;
	this.myCards = new Card[this.numdeCards];// [52]
	int c = 0;

	// nest loop

	for (int d = 0; d < numDeck; d++) {
	    for (int s = 0; s < 4; s++) { // s=suit
		for (int n = 1; n <= 13; n++) { // n= number
		    // index of suits and number.

		    this.myCards[c] = new Card(Suit.values()[s], n);
		    c++;
		}
	    }
	}
	// shuffle
	if (shuffle == true) {
	    this.shuffle();
	}
    }

    public void shuffle() {

	Random rd = new Random();
	Card y;
	int j;
	for (int i = 0; i < this.numdeCards; i++) {

	    j = rd.nextInt(this.numdeCards);// from 0 to 51
	    y = this.myCards[i];
	    // SWAPER
	    this.myCards[i] = this.myCards[j];
	    this.myCards[j] = y;

	}
    }

    public Card dealNextCard() {
	Card top = this.myCards[0];
	// shift to the left
	for (int c = 1; c < this.numdeCards; c++) {
	    this.myCards[c -1] = this.myCards[c];
	}
	this.myCards[this.numdeCards - 1] = null;
	this.numdeCards--; // decrement of cards in the Deck
	return top;
    }

    public void printDeck(int ntoPrint) {
	for (int c = 0; c < ntoPrint; c++) {

	    System.out.printf("%3d/%d %s\n"  ,c+1, this.numdeCards, this.myCards[c].toString());

	}
	  System.out.println( "LE RESTE DE CARTES: " +(this.numdeCards - ntoPrint));

    }
}
