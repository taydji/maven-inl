
public class Card {

    public Suit mySuit;
    public int MyNum; // vi kan göra den public

    // constructeur de cartes
    public Card(Suit aSuit, int aNum) {

	this.mySuit = aSuit; // accéder au data
	this.MyNum = aNum;
// check th numbers are valid
	if (aNum >= 1 && aNum <= 13) {
	    this.MyNum = aNum;

	} else {
	    System.err.println(MyNum + " is not valid");
	    System.exit(1);
	}

    }

    // une methode getnumber pour avoir le numéro de cartes
    public int getNum() { // om variablen är public s så behöver vi ingen return statment
	return MyNum;

    }

    // object calss to String method
    // string métode qui return les noms des cartes.
    public String toString() {

	String numStr = "";

	switch (this.MyNum) {

	case 2:
	    numStr = "Two";
	    break;

	case 3:
	    numStr = "Three";
	    break;
	case 4:
	    numStr = "Four";
	    break;
	case 5:
	    numStr = "Five";
	    break;

	case 6:
	    numStr = " Six";
	    break;

	case 7:
	    numStr = "Seven";

	    break;

	case 8:

	    numStr = "Eight";
	    break;

	case 9:
	    numStr = "Nine";
	case 10:

	    numStr = "Ten";
	    break;

	case 11:

	    numStr = "Jack";
	    break;

	case 12:

	    numStr = "Queen";
	    break;

	case 13:

	    numStr = "King";
	    break;

	case 1:

	    numStr = "Ace";
	    break;

	}
	return numStr + " av " + mySuit.toString();
    }

}
